#!/bin/bash

usage() {
  cat <<EOF
Usage: $0 [options]

Runs GitLab License Management on the application.

  -h, --help                 Display this help and exit.
EOF
}
getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi
OPTIONS=h
LONGOPTS=help
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 2
fi
eval set -- "$PARSED"
h=n
while true; do
  case "$1" in
      -h|--help)
        usage
        exit 1
        ;;
      --)
        shift
        break
        ;;
      *)
        echo "Programming error"
        exit 3
        ;;
  esac
done

projectdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../
cd $projectdir

docker run \
  -it \
  --rm \
  -v $projectdir/application:/code \
  registry.gitlab.com/gitlab-org/security-products/license-management analyze /code

docker run \
  -it \
  --rm \
  -v $projectdir:/project \
  -w /project \
  alpine \
  /bin/sh -c \
    "rm -r application/doc \
    && mv application/gl-license-management-report.json gl-license-scanning-report.json"
