[![pipeline status](https://gitlab.com/pauljdehmer/ulam-spiral/badges/master/pipeline.svg)](https://gitlab.com/pauljdehmer/ulam-spiral/-/commits/master)
[![coverage report](https://gitlab.com/pauljdehmer/ulam-spiral/badges/master/coverage.svg)](https://gitlab.com/pauljdehmer/ulam-spiral/-/commits/master)

# Ulam Spiral

_A [Ulam spiral](https://en.wikipedia.org/wiki/Ulam_spiral) generator._

## Dependencies

This project requires [Docker](https://www.docker.com/) to be installed.

## Usage

There are scripts included in the ```script``` directory of this project.

* ```bash.sh```
* ```build.sh```
* ```lint.sh```
* ```serve.sh```
* ```test.sh```

These scripts start or enter a docker container (if already running) with
the ```application``` folder mounted as a volume and execute commands on the
application. Run any of these with the ```--help``` option for more information
about them.

The ```gitlab-*``` scripts run tools and scanners provided by Gitlab on the
project. These are also run in the pipeline. Run any of these with the ```--help```
option for more information about them.

## Application

A [Ulam spiral](https://en.wikipedia.org/wiki/Ulam_spiral) generator.

## Demo

<https://pauljdehmer.gitlab.io/ulam-spiral/>