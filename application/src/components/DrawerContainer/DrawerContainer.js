import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import React from "react";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import ulam1 from "../../assets/images/Ulam_spiral_howto_all_numbers.svg";
import ulam2 from "../../assets/images/Ulam_spiral_howto_primes_only.svg";
import { useState } from "react";

const useStyles = makeStyles({
  list: {
    width: 350
  },
  content: {
    margin: "12px 25px"
  },
  button: {
    margin: "10px"
  },
  images: {
    display: "block",
    width: "65%",
    margin: "0 auto"
  }
});

function DrawerContainer() {
  const classes = useStyles();
  const [drawerOpen, setDrawerOpen] = useState(false);
  const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);

  const toggleDrawer = open => event => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setDrawerOpen(open);
  };

  return (
    <div>
      <Button
        size="small"
        className={classes.button}
        variant="contained"
        onClick={toggleDrawer(true)}
      >
        Info
      </Button>
      <SwipeableDrawer
        disableBackdropTransition={!iOS}
        disableDiscovery={iOS}
        open={drawerOpen}
        onOpen={toggleDrawer(true)}
        onClose={toggleDrawer(false)}
      >
        <div
          className={classes.list}
          role="presentation"
          onKeyDown={toggleDrawer(false)}
        >
          <AppBar position="static">
            <Toolbar>
              <Typography variant="h6" color="inherit">
                Ulam Spiral
              </Typography>
            </Toolbar>
          </AppBar>
          <div className={classes.content}>
            <Typography variant="body2" component="div" gutterBottom>
              <p>
                A{" "}
                <a
                  href="https://en.wikipedia.org/wiki/Ulam_spiral"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  Ulam Spiral
                </a>{" "}
                generator.
              </p>
              <p>
                The Ulam spiral or prime spiral is a graphical depiction of the
                set of prime numbers, devised by mathematician Stanisław Ulam in
                1963.
              </p>
              <p>
                The Ulam spiral is constructed by writing the positive integers
                in a spiral arrangement on a square lattice:
              </p>
              <img className={classes.images} src={ulam1} alt="logistic map" />
              <p>and then marking the prime numbers:</p>
              <img className={classes.images} src={ulam2} alt="logistic map" />
            </Typography>
          </div>
        </div>
      </SwipeableDrawer>
    </div>
  );
}

export default DrawerContainer;
