import Canvas from "../Canvas/Canvas";
import DrawerContainer from "../DrawerContainer/DrawerContainer";
import React from "react";

function Layout() {
  return (
    <div>
      <Canvas />
      <DrawerContainer />
    </div>
  );
}

export default Layout;
