import * as Primes from "./Primes";
import * as SprialCoordinates from "./SpiralCoordinates";

import Particle from "./Particle";
import React from "react";
import Tooltip from "@material-ui/core/Tooltip";
import { makeStyles } from "@material-ui/core/styles";
import { useEffect } from "react";
import { useRef } from "react";
import { useState } from "react";

const useStyles = makeStyles(theme => ({
  canvasContainer: {
    backgroundColor: "#333",
    height: "100%",
    left: 0,
    position: "fixed",
    top: 0,
    width: "100%"
  },
  toolTip: {
    position: "relative"
  }
}));

const useStylesToolTipFont = makeStyles(theme => ({
  tooltip: {
    fontSize: "1rem"
  }
}));

function Canvas() {
  const classes = useStyles();
  const toolTipFontClasses = useStylesToolTipFont();
  const canvasContainer = useRef(null);
  const canvas = useRef(null);
  const toolTip = useRef(null);
  const [clientDimensions, setClientDimensions] = useState([0, 0]);
  const requestRef = useRef();
  const [toolTipOpen, setToolTipOpen] = React.useState(false);
  const [mousePosition, setMousePosition] = React.useState([0, 0]);
  const [toolTipNumber, setToolTipNumber] = React.useState(0);
  const primeParticles = useRef();

  useEffect(() => {
    let width, height, devicePixelRatio;

    const getClientDimensions = () => {
      devicePixelRatio = window.devicePixelRatio;
      width = canvasContainer.current.clientWidth * devicePixelRatio;
      height = canvasContainer.current.clientHeight * devicePixelRatio;
      return [width, height, devicePixelRatio];
    };

    const handleResize = () => {
      setClientDimensions(getClientDimensions());
    };

    handleResize();
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  useEffect(() => {
    const particleSize = 10 * clientDimensions[2];
    const fps = 100;
    const fpsInterval = 1000 / fps;

    let ctx,
      totalNumbers,
      coordinates,
      now,
      then = Date.now(),
      elapsed;

    primeParticles.current = [];

    ctx = canvas.current.getContext("2d");
    ctx.globalAlpha = 0.8;
    ctx.fillStyle = "#fff";

    const draw = () => {
      totalNumbers =
        (Math.max(
          ctx.canvas.width / particleSize,
          ctx.canvas.height / particleSize
        ) +
          1) *
        Math.max(
          ctx.canvas.width / particleSize,
          ctx.canvas.height / particleSize
        );

      for (let i = 0; i < totalNumbers; i++) {
        if (Primes.isPrime(i)) {
          primeParticles.current.push(
            new Particle(
              i,
              ctx,
              [ctx.canvas.width / 2, ctx.canvas.height / 2],
              particleSize,
              particleSize
            )
          );
        }
      }

      coordinates = SprialCoordinates.getSpiralCoordinates(totalNumbers);
      for (let i = 0; i < primeParticles.current.length; i++) {
        primeParticles.current[i].setNewPosition(Date.now(), [
          coordinates[primeParticles.current[i].number - 1][0] * particleSize +
            ctx.canvas.width / 2,
          -(
            coordinates[primeParticles.current[i].number - 1][1] * particleSize
          ) +
            ctx.canvas.height / 2
        ]);
      }

      requestRef.current = requestAnimationFrame(step);
    };

    const step = time => {
      //https://stackoverflow.com/questions/19764018/controlling-fps-with-requestanimationframe
      now = Date.now();
      elapsed = now - then;

      if (elapsed > fpsInterval) {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        for (let i = 0; i < primeParticles.current.length; i++) {
          primeParticles.current[i].step(now);
        }
        then = now - (elapsed % fpsInterval);
      }

      requestRef.current = requestAnimationFrame(step);
    };

    draw();

    return () => {
      cancelAnimationFrame(requestRef.current);
    };
  }, [clientDimensions]);

  useEffect(() => {
    const handleMouseMove = event => {
      let mouseIsOverPrime = false;
      setMousePosition([
        event.clientX * devicePixelRatio,
        event.clientY * devicePixelRatio
      ]);

      for (let i = 0; i < primeParticles.current.length; i++) {
        if (
          primeParticles.current[i].isPointInside(
            mousePosition[0],
            mousePosition[1]
          )
        ) {
          mouseIsOverPrime = true;
          setToolTipNumber(primeParticles.current[i].number);
        }
      }

      if (mouseIsOverPrime) {
        setToolTipOpen(true);
      } else {
        setToolTipOpen(false);
      }
    };

    const handleTouch = event => {
      let mouseIsOverPrime = false;
      setMousePosition([
        event.touches[0].clientX * devicePixelRatio,
        event.touches[0].clientY * devicePixelRatio
      ]);

      for (let i = 0; i < primeParticles.current.length; i++) {
        if (
          primeParticles.current[i].isPointInside(
            mousePosition[0],
            mousePosition[1]
          )
        ) {
          mouseIsOverPrime = true;
          setToolTipNumber(primeParticles.current[i].number);
        }
      }

      if (mouseIsOverPrime) {
        setToolTipOpen(true);
      } else {
        setToolTipOpen(false);
      }
    };

    window.addEventListener("mousemove", handleMouseMove);
    window.addEventListener("touchstart", handleTouch);
    window.addEventListener("touchmove", handleTouch);
    return () => {
      window.removeEventListener("mousemove", handleMouseMove);
      window.removeEventListener("touchstart", handleTouch);
      window.removeEventListener("touchmove", handleTouch);
    };
  }, [mousePosition]);

  return (
    <div className={classes.canvasContainer} ref={canvasContainer}>
      <Tooltip
        classes={toolTipFontClasses}
        PopperProps={{
          disablePortal: true
        }}
        arrow
        open={toolTipOpen}
        disableFocusListener
        disableHoverListener
        disableTouchListener
        title={"Prime: " + toolTipNumber}
      >
        <div
          className={classes.toolTip}
          ref={toolTip}
          style={{
            left:
              (mousePosition[0] - clientDimensions[0] / 2) / devicePixelRatio,
            top: mousePosition[1] / devicePixelRatio - 10
          }}
        />
      </Tooltip>
      <canvas
        ref={canvas}
        width={clientDimensions[0]}
        height={clientDimensions[1]}
        style={{ width: "100%", height: "100%" }}
      />
    </div>
  );
}

export default Canvas;
