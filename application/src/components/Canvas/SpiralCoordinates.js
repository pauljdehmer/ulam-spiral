//https://stackoverflow.com/questions/398299/looping-in-a-spiral
const getSpiralCoordinates = totalCoordinates => {
  let x = 0;
  let y = 0;
  let dx = 0;
  let dy = -1;
  let dxHolder = 0;
  let dyHolder = 0;
  let coordinates = [];

  for (let i = 0; i < totalCoordinates; i++) {
    coordinates.push([x, y]);
    if (x === y || (x < 0 && x === -y) || (x > 0 && x === 1 - y)) {
      dxHolder = -dy;
      dyHolder = dx;
      dx = dxHolder;
      dy = dyHolder;
    }
    x = x + dx;
    y = y + dy;
  }

  return coordinates;
};

export { getSpiralCoordinates };
