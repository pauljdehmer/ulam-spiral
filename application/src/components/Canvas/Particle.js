const easeInOutQuart = t => {
  return t < 0.5 ? 8 * t * t * t * t : 1 - 8 * --t * t * t * t;
};

class Particle {
  constructor(number, ctx, coordinates, xSize, ySize) {
    this.number = number;
    this.ctx = ctx;
    this.xPos = coordinates[0];
    this.yPos = coordinates[1];
    this.xSize = xSize;
    this.ySize = ySize;
    this.initialTimestamp = 0;
    this.startXPosition = this.xPos;
    this.startYPosition = this.yPos;
    this.endXPosition = this.xPos;
    this.endYPosition = this.yPos;
    this.speed = 5;
    this.ctx.fillRect(this.xPos, this.yPos, this.xSize, this.ySize);
  }

  step = timestamp => {
    let timeDifference =
      (timestamp - this.initialTimestamp) * (this.speed / 10000) < 1
        ? (timestamp - this.initialTimestamp) * (this.speed / 10000)
        : 1;
    timeDifference = timeDifference < 0 ? 0 : timeDifference;
    let easingMultiplier = easeInOutQuart(timeDifference);
    this.xPos =
      (this.endXPosition - this.startXPosition) * easingMultiplier +
      this.startXPosition;
    this.yPos =
      (this.endYPosition - this.startYPosition) * easingMultiplier +
      this.startYPosition;
    this.ctx.fillRect(this.xPos, this.yPos, this.xSize, this.ySize);
  };

  setNewPosition = (timestamp, coordinates) => {
    this.initialTimestamp = timestamp + Math.random() * 500;
    this.startXPosition = this.xPos;
    this.startYPosition = this.yPos;
    this.endXPosition = coordinates[0];
    this.endYPosition = coordinates[1];
  };

  isPointInside = function(x, y) {
    return (
      x >= this.xPos &&
      x <= this.xPos + this.xSize &&
      y >= this.yPos &&
      y <= this.yPos + this.ySize
    );
  };
}

export default Particle;
